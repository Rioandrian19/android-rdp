LOCAL_PATH := $(call my-dir)

#
## librdpjni.so
#
include $(CLEAR_VARS)
LOCAL_MODULE    := rdpjni
LOCAL_SRC_FILES := \
		android_graphics.cpp \
		android_freerdp.cpp \
		jniUtils.cpp		\
		com_softmedia_rdp_FreeRDP.cpp		

LOCAL_LDLIBS := -llog -lz -ljnigraphics #-L$(LOCAL_PATH)/../libs/ -lcrypto -lssl

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH) \
    $(LOCAL_PATH)/../FreeRDP/include \
    $(LOCAL_PATH)/../FreeRDP/winpr/include \

LOCAL_SRC_FILES += \
		../FreeRDP/client/Android/jni/android_event.c \
		../FreeRDP/client/Android/jni/android_freerdp.c \
		../FreeRDP/client/Android/jni/android_jni_callback.c \
		../FreeRDP/client/Android/jni/generated/android_freerdp_jni.c

LOCAL_C_INCLUDES += \
		$(LOCAL_PATH)/../FreeRDP/client/Android/jni \
		$(LOCAL_PATH)/../FreeRDP/client/Android/jni/generated
		
LOCAL_STATIC_LIBRARIES := freerdp
LOCAL_STATIC_LIBRARIES += avutil avcodec avutil
LOCAL_STATIC_LIBRARIES += crypto ssl crypto
LOCAL_STATIC_LIBRARIES += jpeg
LOCAL_STATIC_LIBRARIES += cpufeatures

include $(LOCAL_PATH)/../android-ndk-profiler-3.1/android-ndk-profiler.mk
include $(BUILD_SHARED_LIBRARY)
