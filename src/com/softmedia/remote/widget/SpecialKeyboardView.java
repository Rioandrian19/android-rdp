package com.softmedia.remote.widget;

import android.content.Context;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;

public class SpecialKeyboardView extends KeyboardView {

    public SpecialKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpecialKeyboardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
